package gatway

import (
	"encoding/json"
	"fmt"
)

type Worker struct {
	Gateway *Gatway
}

func (p *Worker) OnConnect(clientId string) {
	p.Gateway.SendToAll(fmt.Sprintf("%s连接成功", clientId))
}

func (p *Worker) OnMessage(clientId string, data []byte) {
	var message map[string]interface{}
	if err := json.Unmarshal(data, &message); err != nil {
		fmt.Println("参数错误")
	}
	fmt.Println(message)
}

func (p *Worker) OnClose(clientId string) {
	fmt.Println(fmt.Sprintf("%s-断开", clientId))
}
