package main

import (
	"fmt"
	"net/http"
	"vueadmin/gatway"
)

func main() {
	event := &gatway.Worker{}
	gateway := gatway.New(event)
	event.Gateway = gateway
	http.HandleFunc("/", gateway.Run)
	err := http.ListenAndServe(":7777", nil)
	if err != nil {
		fmt.Println(err)
	}
}
