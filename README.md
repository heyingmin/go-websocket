# go-websocket
------------------------

## 强调
本系统基于go开发，底层实现完全使用go官方websocket库，业务层模式完全仿照workman官方的 gatwayworker实现  
联系qq：872977817

## 基本说明
链接地址  ws://127.0.0.1:7777  
使用基本跟gatwayworker一致 可以参照它的官方文档 https://www.workerman.net/doc/gateway-worker/

### demo
``` 
package websocket

import (
	"encoding/json"
	"fmt"
	"github.com/vueadmin/pkg/gatway"
	"github.com/vueadmin/utils/conv"
	"net/http"
)

type Worker struct {
	Gateway *gatway.Gatway
}


func (p *Worker) OnConnect(clientId string) {
	p.Gateway.SendToAll(fmt.Sprintf("%s连接成功", clientId))
}

func (p *Worker) OnMessage(clientId string, data []byte) {
	var message map[string]interface{}
	if err := json.Unmarshal(data, &message); err != nil {
		fmt.Println("参数错误")
	}
	fmt.Println(message)
	req := message["data"]
	switch message["event"] {
	case "bindUid": //链接绑定uid
		if err := p.Gateway.BindUid(clientId, conv.String(req)); err != nil {
			fmt.Println(err)
		}
	case "unBindUid": //链接同uid解绑
		if err := p.Gateway.UnBindUid(conv.String(req)); err != nil {
			fmt.Println(err)
		}
	case "sendUid": //向指定的uid发送一条消息
		if err := p.Gateway.SendToUid(conv.String(req), "指定uid发送消息"); err != nil {
			fmt.Println(err)
		}
	case "isUidOnline": //判断uid是否在线
		res := p.Gateway.IsUidOnline(conv.String(req))
		if res {
			fmt.Println("uid在线")
		}
	case "isClientIdOnline": //根据client_id判断是否在线
		res := p.Gateway.IsOnline(clientId)
		if res {
			fmt.Println("client在线")
		}
	case "getUidByClientId": //通过uid获取client_id
		uid, err := p.Gateway.GetUidByClientId(clientId)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(uid)
	case "closeClient": //关闭连接
		if err := p.Gateway.CloseClient(clientId); err != nil {
			fmt.Println(err)
		}
	case "joinGroup": //加入一个分组
		if err := p.Gateway.JoinGroup(conv.String(req), clientId); err != nil {
			fmt.Println(err)
		}
	case "leaveGroup": //离开一个分组
		if err := p.Gateway.LeaveGroup(conv.String(req), clientId); err != nil {
			fmt.Println(err)
		}
	case "getCountByGroup": //获取当前分组下的所有链接数量
		num, err := p.Gateway.GetClientIdCountByGroup(conv.String(req))
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(num)
	case "sendToGroup": //向当前分组的所有成员发送消息
		err := p.Gateway.SendToGroup(conv.String(req), "欢迎加入分组")
		if err != nil {
			fmt.Println(err)
		}
	case "getAllClientIdCount": //获取所有的链接在线的数量
		num := p.Gateway.GetAllClientIdCount()
		fmt.Println(num)
	case "getGroupsByClientId": //通过当前client_id获取加入的所有分组
		list, err := p.Gateway.GetGroupsByClientId(clientId)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(list)
	case "getClientIdListByGroup": //通过当前分组获取当前分组下所有的client_id
		list, err := p.Gateway.GetClientIdListByGroup(conv.String(req))
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(list)
	case "getAllClientIdList": //获取所有的链接在线的client_id
		list, err := p.Gateway.GetAllClientIdList()
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(list)
	case "getUidListByGroup": //通过groupId获取当前分组下所有的uid
		list, err := p.Gateway.GetUidListByGroup(conv.String(req))
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(list)
	case "getUidCountByGroup": //通过groupId获取当前分组下所有的uid数量
		num, err := p.Gateway.GetUidCountByGroup(conv.String(req))
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(num)
	case "getAllUidList": //获取链接绑定了uid所有的uid列表
		list := p.Gateway.GetAllUidList()
		fmt.Println(list)
	case "getAllUidCount": //获取链接绑定了uid所有的uid总数
		num := p.Gateway.GetAllUidCount()
		fmt.Println(num)
	case "getAllGroupIdList": //获取所有的groupId列表
		list := p.Gateway.GetAllGroupIdList()
		fmt.Println(list)
	case "setSession": //给指定链接设置绑定参数信息 如 map["name"] = "寒塘冷月"
		msg := conv.StringToObject(conv.String(req))
		err := p.Gateway.SetSession(clientId, msg)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(msg)
	case "getSession": //获取当前链接绑定的参数
		info, err := p.Gateway.GetSession(clientId)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("session值", info)
	case "getAllClientSessions": //获取所有链接绑定的参数列表
		list := p.Gateway.GetAllClientSessions()
		fmt.Println(list)
	case "getClientSessionsByGroup": //获取当前分组下链接绑定的参数列表
		list, err := p.Gateway.GetClientSessionsByGroup(conv.String(req))
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(list)
	}
}

func (p *Worker) OnClose(clientId string) {
	fmt.Println(fmt.Sprintf("%s-断开", clientId))
}

```

服务端运行
------------------------

``` 
# 安装依赖包
go mod tidy

# 启动项目
go run main.go

```
